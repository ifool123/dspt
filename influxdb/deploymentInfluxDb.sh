#清理influxdb相关配置
echo '清理influxdb相关配置'
kubectl delete service influxdb-service
kubectl delete deployment influxdb-deployment

echo '创建influxdb服务'
kubectl create -f influxdb_deployment.yml
kubectl create -f influxdb_service.yml

echo '创建成功, 数据库接口30001,WebUI端口30002'

echo '等待10s'
sleep 10
echo '创建数据库jmeter'
curl -X POST 'http://192.168.174.51:30001/query?q=create+database+%22jmeter%22&db=_internal'
echo '创建数据库telegraf'
curl -X POST 'http://192.168.174.51:30001/query?q=create+database+%22telegraf%22&db=_internal'

