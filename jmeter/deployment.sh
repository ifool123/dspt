
#清理
echo $清理上次测试
kubectl delete pod jmetermaster
kubectl delete cm testplan
kubectl delete cm jmetermasterconfig
kubectl delete deployment jmeter-deployment
rm jmeter_deployment.yml
rm jmeter.properties
sleep 10

#创建jmeter slave
cp jmeter_deployment.yml.org jmeter_deployment.yml
sed -i "s/SLAVES/$2/g" jmeter_deployment.yml
kubectl create -f jmeter_deployment.yml
sleep 10
echo "创建""$2""个jmeter slave"
kubectl get pods | grep jmeter-deployment | grep Running

slaveIp=`kubectl get pod -o wide| grep jmeter-deployment |grep Running| awk -F' ' '{printf $6","}'| awk '{print substr($1,0,length-1)}'`

kubectl create configmap testplan --from-file=$1
cp jmeter.properties.org jmeter.properties
echo "remote_hosts=""$slaveIp" >> jmeter.properties
kubectl create configmap jmetermasterconfig --from-file=./jmeter.properties


kubectl create -f jmeterMaster.yml

sleep 5

kubectl logs jmetermaster

